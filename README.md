# todo-app

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

A simple Todo App written in [FatScript](https://fatscript.org).

## Installation

> you need [fry interpreter](https://gitlab.com/fatscript/fry) ^v3.4.0 installed on your system

Clone this repository:

```bash
git clone https://gitlab.com/aprates/fat-todo-app.git
cd fat-todo-app
```

Then use the provided install script:

```bash
./install.sh
```

OR

Bundle this app like so:

```bash
fry -b todo src/main.fat
```

Copy the resulting bundle to a folder in bin path, e.g.:

```bash
cp -fv todo "$HOME/.local/bin/"
```

## Usage

To start the app in interactive mode:

```bash
todo
```

Or use CLI mode to print a list:

```bash
todo s <list> /p
```

Or add a task:

```bash
todo s <list> /a <task>
```

> the `/` slash char concatenates commands

## License

[MIT](LICENSE) © 2023-2025 Antonio Prates
