#!/usr/bin/env bash

# todo-app project <https://gitlab.com/aprates/fat-todo-app>
# Original author: Antonio Prates
# License: MIT

fry -b todo src/main.fat
cp -fv todo "$HOME/.local/bin/"
echo
todo h
